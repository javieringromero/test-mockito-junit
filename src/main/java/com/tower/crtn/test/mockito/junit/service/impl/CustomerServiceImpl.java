package com.tower.crtn.test.mockito.junit.service.impl;

import com.tower.crtn.test.mockito.junit.component.CosConectorComponent;
import com.tower.crtn.test.mockito.junit.component.KafkaProducer;
import com.tower.crtn.test.mockito.junit.entity.CustomerEntity;
import com.tower.crtn.test.mockito.junit.exception.model.BusinessException;
import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;
import com.tower.crtn.test.mockito.junit.properties.CloudObjectStorageProperties;
import com.tower.crtn.test.mockito.junit.properties.KafkaProperties;
import com.tower.crtn.test.mockito.junit.repository.CustomerRepository;
import com.tower.crtn.test.mockito.junit.service.CustomerService;
import com.tower.crtn.test.mockito.junit.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private CosConectorComponent cosConectorComponent;

  @Autowired
  private CloudObjectStorageProperties cosProperties;

  @Autowired
  private KafkaProducer kafkaProducer;

  @Autowired
  private KafkaProperties kafkaProperties;

  @Override
  public boolean saveCustomer(CustomerModel customer) {
    boolean flag = false;
    try {
      customerRepository.save(Util.converterCustomerModelToCustomerEntity(customer));
      File file = new File("Customer_" + customer.getCustomerId() + "_" + new Date().toInstant());
      cosConectorComponent.uploadFile(cosProperties.getBucketName(), file.getName(), file);
      flag = true;
    } catch (Exception ex) {
      throw new BusinessException("01", "Error to save the customer information");
    }
    return flag;
  }

  @Override
  public boolean updateCustomer(int customerId, CustomerUpdateModel customerModel) {
    boolean flag = false;
    try {
      customerRepository
          .save(Util.converterCustomerUpdateModelToCustomerEntity(customerId, customerModel));
      File file = new File("Customer_" + customerId + "_" + new Date().toInstant());
      cosConectorComponent.uploadFile(cosProperties.getBucketName(), file.getName(), file);
      flag = true;
    } catch (Exception ex) {
      throw new BusinessException("02", "Error to update the customer information");
    }
    return flag;
  }

  @Override
  public boolean deleteCustomer(int customerId) {
    boolean flag = false;
    try {
      CustomerEntity entity = customerRepository.findByCustomerId(customerId);
      entity.setStatusCode("-1");
      customerRepository.save(entity);
      flag = true;
    } catch (Exception ex) {
      throw new BusinessException("03", "Error when unsubscribing the client");
    }
    return flag;
  }

  @Override
  public List<CustomerModel> findAllCustomers() {
    List<CustomerEntity> customersEntity = customerRepository.findAll();
    List<CustomerModel> customersModel = new ArrayList<>();
    customersEntity.stream()
        .forEach(x -> customersModel.add(Util.converterCustomerEntityToCustomerModel(x)));
    kafkaProducer.send(kafkaProperties.getTopicFileReciver(), Util.getJson(customersModel));
    return customersModel;
  }

  @Override
  public CustomerModel findByCustomerId(int customerId) {
    CustomerEntity customerEntity = customerRepository.findByCustomerId(customerId);
    kafkaProducer.send(kafkaProperties.getTopicFileReciver(), Util.getJson(customerEntity));
    return Util.converterCustomerEntityToCustomerModel(customerEntity);
  }

  @Override
  public CustomerModel findByFullName(String name, String lastName) {
    CustomerEntity customerEntity = customerRepository.findByNameAndLastName(name, lastName);
    kafkaProducer.send(kafkaProperties.getTopicFileReciver(), Util.getJson(customerEntity));
    return Util.converterCustomerEntityToCustomerModel(customerEntity);
  }

  @Override
  public List<CustomerModel> findAllByStatusCode(String statusCode) {
    List<CustomerEntity> customersEntity = customerRepository.findByStatusCode(statusCode);
    List<CustomerModel> customersModel = new ArrayList<>();
    customersEntity.stream()
        .forEach(x -> customersModel.add(Util.converterCustomerEntityToCustomerModel(x)));
    kafkaProducer.send(kafkaProperties.getTopicFileReciver(), Util.getJson(customersModel));
    return customersModel;
  }
}
