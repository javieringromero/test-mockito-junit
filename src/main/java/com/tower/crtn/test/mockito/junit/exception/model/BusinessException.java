package com.tower.crtn.test.mockito.junit.exception.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private String code;

  private String details;

  public BusinessException(String code, String details) {
    super(details);
    this.code = code;
    this.details = details;
  }

}
