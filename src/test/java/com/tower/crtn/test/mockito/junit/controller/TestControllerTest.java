package com.tower.crtn.test.mockito.junit.controller;

import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootApplication
public class TestControllerTest {

  @InjectMocks
  private TestController testController;

  @Mock
  private CustomerService customerService;

  private CustomerModel customerModel;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);

    customerModel = new CustomerModel();
    customerModel.setCustomerId(1);
    customerModel.setName("Crtn");
    customerModel.setLastName("Tower");
    customerModel.setBirthDate("11-11-1994");
    customerModel.setStatusCode("00");
  }

  @Test
  public void saveCustomer() {
    Mockito.when(customerService.saveCustomer(Mockito.any())).thenReturn(true);
    ResponseEntity<Void> response = testController.saveCustomer(customerModel);
    Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCodeValue());
  }

}
